window.addEventListener('keypress', (event) => {
    $('.btn').each((i, elem ) => {
        if ((elem.innerText).toUpperCase() === (event.key).toUpperCase()) {
            $(elem).css("background-color", "blue");
        } else {
            $(elem).css("background-color", '#33333a');
        }
    });
 });


// console.log(`keyup
// keyCode - ${event.keyCode},
//     key - ${event.key},
//     which - ${event.which},
//     charCode - ${event.charCode},
//     code - ${event.code}`);
// console.dir(event);
